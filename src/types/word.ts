export enum WORD_ERROR_TYPES {
	USED = 'This word has already been',
	REGEX = 'Wrong word',
	NON_EXIST = 'Such a word does not exist',
	RULE = 'The game is not by the rules, the word must begin with'
}

export type WordComputerItems = (string | WORD_ERROR_TYPES)[]
export type WordErrorItems = boolean[]
export type WordItems = string[]