import React from 'react'

import './loading.css'

export const CircleLoading: React.FC = () => {

	return (
		<div className="loading loading--circle">
			<div className="loading--circle__spin"/>
		</div>
	)
}


