import React, { useMemo, useState } from 'react'
import { WORD_ERROR_TYPES, WordItems, WordComputerItems, WordErrorItems } from '../../types'
import { MIN_WORD_LENGTH, prepareString, isStringValid, validateWord, generateWord } from '../../config'

import './app.css'
import { CircleLoading } from '../loading'
import { unstable_renderSubtreeIntoContainer } from 'react-dom'

export const App: React.FC = () => {
	const [ lastComputerLetter, setLastComputerLetter ] = useState<string>('')
	const [ userWords, setUserWords ] = useState<WordItems>([])
	const [ computerWords, setComputerWords ] = useState<WordComputerItems>([])
	const [ usedWords, setUsedWords ] = useState<WordItems>([])
	const [ errors, setErrors ] = useState<WordErrorItems>([])
	const [ enteredValue, setEnteredValue ] = useState<string>('')
	const [ loading, setLoading ] = useState(false)
	const isEnteredValueValid = useMemo(() => {
		return enteredValue?.length >= MIN_WORD_LENGTH
	}, [ enteredValue ])

	async function handleUserValueSubmit() {
		setLoading(true)
		const word = prepareString(enteredValue)
		setUserWords(userWords.concat(word))
		let error
		switch (true) {
			case !isStringValid(word):
				error = WORD_ERROR_TYPES.REGEX
				break
			case lastComputerLetter && word.slice(0, 1) !== lastComputerLetter:
				error = WORD_ERROR_TYPES.RULE + ' "' + lastComputerLetter + '"'
				break
			case userWords.includes(word) || computerWords.includes(word):
				error = WORD_ERROR_TYPES.USED
				break
			case !await validateWord(word):
				error = WORD_ERROR_TYPES.NON_EXIST
				break
		}
		if ( error ) {
			setComputerWords(computerWords.concat(error))
			setErrors(errors.concat(true))
		} else {
			const computerWord = await generateWord(word.slice(-1), usedWords.concat(word))
			setUsedWords(usedWords.concat([word, computerWord]))
			setErrors(errors.concat(false))
			setLastComputerLetter(computerWord.slice(-1))
			setComputerWords(computerWords.concat(computerWord))
			setEnteredValue('')
		}
		setLoading(false)
	}

	async function handleEnterPress(event: React.KeyboardEvent) {
		if ( event.key === 'Enter' && isEnteredValueValid ) {
			await handleUserValueSubmit()
		}
	}

	return (
		<div className="root-container">
		<div className="container container--left">
			<h1 className="container__title">Word Game</h1>
			<p className="container__brief">Try to defeat me!</p>
			<div className="container__wrapper">
				<div className="words">
					{ userWords.map((word, index) => (
						<div className="words__item" data-word-invalid={ errors[index] } key={ index }>
							<pre className="words__item__num">{ index + 1 }.</pre>
							<span className="words__item__text">{ word }</span>
						</div>
					)) }
				</div>
				<div className="words">
					{ computerWords.map((word, index) => (
						<div className="words__item words__item--computer" key={ index }>
							<pre className="words__item__num">{ index + 1 }.</pre>
							<span className="words__item__text">{ word }</span>
						</div>
					)) }
					{loading && <div className="words__item"><CircleLoading/></div>}
				</div>
			</div>
			<div className="form">
				<input
					className="form__input"
					type="text"
					name="new-user-word"
					value={ enteredValue }
					placeholder="Enter your word"
					onChange={ (e: React.ChangeEvent<HTMLInputElement>) => setEnteredValue(e.target.value) }
					onKeyPress={ handleEnterPress }
				/>
				<button
					className="form__btn"
					type="submit"
					onClick={ handleUserValueSubmit }
					disabled={ !isEnteredValueValid }
				>
					submit
				</button>
			</div>
		</div>
			<div className="container container--right">
				<section className="section">
				<h2 className="section__title">Goal</h2>
				<p className="section__paragraph">Play until the computer can't make its move, or you can't come up with a word for a given letter.</p>
				</section>
				<section className="section">
				<h2 className="section__title">Rules</h2>
				<ul className="section__list">
					<li>write the word on the letter that ends with the last word of the computer;</li>
					<li>use only existing words;</li>
					<li>the word must be two or more characters long;</li>
					<li>write all words in lowercase;</li>
					<li>do not use special characters or numbers.</li>
				</ul>
				<strong className="section__paragraph">Good luck!</strong>
				</section>
			</div>
		</div>
	)
}
