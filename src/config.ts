//------------------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------------------

import { WordComputerItems, WordItems } from './types'

export const MIN_WORD_LENGTH = 2
const regexOnlyAlphabets = /^[a-z-]*$/
const wordnikToken = "iw93fy9mxd6bupvfo01r96wgd8wi91rlw6mcqm9fa4haueq9f"
const rapidToken = "6a43b823f5mshd756645b22f507ep17f19bjsn6797339c4c51"

//------------------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------------------

export function isStringValid(str: string) {
	return regexOnlyAlphabets.test(str)
}

export function prepareString(str: string) {
	return str.replace(/\s/g, "").toLowerCase()
}

function timeout(ms: number) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

export async function validateWord(word: string): Promise<boolean> {
	let response
	do {
		const url = `https://api.wordnik.com/v4/word.json/${ word }/definitions?api_key=${ wordnikToken }`
		response = await fetch(url)
		if ( response.status === 429 ) await timeout(1000)
	} while ( response.status === 429 )
	return response.ok
}

export async function generateWord(firstLetter: string, usedWords: WordComputerItems | WordItems): Promise<string> {
	let words: WordItems = []
	do {
		const response = await fetch("https://random-words5.p.rapidapi.com/getMultipleRandom?count=20", {
			method: "GET",
			headers: {
				"x-rapidapi-host": "random-words5.p.rapidapi.com",
				"x-rapidapi-key": rapidToken
			}
		})
		if ( response.ok ) {
			const data = await response.json().then(data => data as WordItems)
			words = data.filter(word => word.toLowerCase().slice(0, 1) === firstLetter && !usedWords.includes(word))
		} else {
			await timeout(1000)
		}
	} while ( !words.length )
	return words[0].toLowerCase()
}
